from abc import ABC, abstractclassmethod
class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass
    @abstractclassmethod
    def addRequest(self):
        pass
    @abstractclassmethod
    def checkRequest(self):
        pass
    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        # super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    #Setters
    def setFirstName(self, firstName):
        self._firstName = firstName
    def setLastName(self, lastName):
        self._lastName = lastName
    def setEmail(self, email):
        self._email = email
    def setDepartment(self, department):
        self._department = department

    #Getters
    def getFirstName(self):
        return self._firstName
    def getLastName(self):
        return self._lastName
    def getEmail(self):
        return self._email
    def getDepartment(self):
        return self._department
    
    #Methods 
    def login(self):
        return f"{self._email} has logged in"
    def logout(self):
        return f"{self._email} has logged out"
    
    #Abstract Methods
    def checkRequest(self):
        pass
    def addUser(self):
        pass
    def addRequest(self):
        return "Request has been added"
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = set()
    
    #Setters
    def setFirstName(self, firstName):
        self._firstName = firstName
    def setLastName(self, lastName):
        self._lastName = lastName
    def setEmail(self, email):
        self._email = email
    def setDepartment(self, department):
        self._department = department

    #Getters
    def getFirstName(self):
        return self._firstName
    def getLastName(self):
        return self._lastName
    def getEmail(self):
        return self._email
    def getDepartment(self):
        return self._department
    
    #Methods 
    def login(self):
        print(f"{self._email} has logged in.")
    def logout(self):
        print(f"{self._email} has logged out.")
    def addMember(self, employee):
        self._members.add(employee)
    def getMembers(self):
        return self._members
    
    #Abstract Methods
    def checkRequest(self):
        pass
    def addUser(self):
        pass
    def addRequest(self):
        return "Request has been added"
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    #Setters
    def setFirstName(self, firstName):
        self._firstName = firstName
    def setLastName(self, lastName):
        self._lastName = lastName
    def setEmail(self, email):
        self._email = email
    def setDepartment(self, department):
        self._department = department

    #Getters
    def getFirstName(self):
        return self._firstName
    def getLastName(self):
        return self._lastName
    def getEmail(self):
        return self._email
    def getDepartment(self):
        return self._department
    
    #Methods 
    def login(self):
        print(f"{self._email} has logged in.")
    def logout(self):
        print(f"{self._email} has logged out.")

    #Abstract Methods
    def checkRequest(self):
        pass
    def addUser(self):
        return "User has been added"
    def addRequest(self):
        pass
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

class Request():
    def __init__(self, name, requester, dateRequested):
        super().__init__()
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = ''
    
    #Setters
    def setName(self, name):
        self._name = name
    def setRequester(self, requester):
        self._requester = requester
    def setDateRequested(self, dateRequested):
        self._dateRequested = dateRequested
    def setStatus(self, status):
        self._status = status
        
    #Getters
    def getName(self):
        return self._name
    def getRequester(self):
        return self._requester
    def getDateRequested(self):
        return self._dateRequested
    def getStatus(self):
        return self._status

    #Methods
    def updateRequest(self):
        pass
    def closeRequest(self):
        return f"Request {self._name} has been closed"
    def cancelRequest(self):
        pass

#Test cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())
assert admin1.addUser() == "User has been added"
req2.setStatus("closed")
print(req2.closeRequest())